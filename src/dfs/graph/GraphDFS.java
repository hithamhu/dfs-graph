/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dfs.graph;

import java.util.Stack;

/**
 *
 * @author Dream Tech
 */
public class GraphDFS {
    //node number عدد النود
    int size;
    
    AdjLinkedList[] array;

    public GraphDFS(int size) {
        this.size=size;
        array=new AdjLinkedList[size];
        for(int i=0;i<size;i++){
            array[i]=new AdjLinkedList();
            array[i].head=null;
        }
        
    }
    /*
    src is node
    dest value of node
    
    */
    public void addNode(int src,int dest){
        Node newNode=new Node(dest, null);
        //where this node shoud pointer
        newNode.next=array[src].head;
        array[src].head=newNode;
        
    }
    
    /*
    startVertix is node where should start
    */
    public void DFSExplore(int startVertix){
        boolean[] visited=new boolean[size];
        Stack<Integer> stack=new Stack<>();
        //add to stack
        stack.push(startVertix);
        while (!stack.isEmpty()) {
            int n=stack.pop();
            stack.push(n);
            visited[n]=true;
            Node head=array[n].head;
            //to ensure all list is visited 
            boolean isDone=true;
            while(head!=null){
                //ensure this value not visited befor
                if(visited[head.dest]==false){
                    stack.push(head.dest);
                    visited[head.dest]=true;
                    isDone=false;
                    break;
                }else{
                    //if this node visited move to next node
                    head=head.next;
                }
            }
            if(isDone==true){
                int outNode=stack.pop();
                System.out.println("visited node: "+outNode);
            }
        }
        
    }
    
    
      public void DFSearsh(int startVertix,int dst){
          boolean isFound=false;
        boolean[] visited=new boolean[size];
        Stack<Integer> stack=new Stack<>();
        //add to stack
        stack.push(startVertix);
        while (!stack.isEmpty()) {
            int n=stack.pop();
            stack.push(n);
            visited[n]=true;
            Node head=array[n].head;
            //to ensure all list is visited 
            boolean isDone=true;
            while(head!=null){
                if(head.dest==dst){
                    System.out.println(head.dest+ " :node is found");
                    //stop while
                    isFound=true;
                    break;
                }
                //ensure this value not visited befor
                if(visited[head.dest]==false){
                    stack.push(head.dest);
                    visited[head.dest]=true;
                    isDone=false;
                    break;
                }else{
                    //if this node visited move to next node
                    head=head.next;
                }
            }
            if(isFound==true){
                break;            }
            if(isDone==true){
                int outNode=stack.pop();
               // System.out.println("visited node: "+outNode);
            }
            
            if(isFound==false){
                System.err.println("not found: "+dst);
            }
        }
        
    }
    
    
    
}
